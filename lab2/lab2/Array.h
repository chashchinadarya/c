#pragma once
#include "pch.h"
#include <iostream>
using namespace std;
class Array
{
private:
	int *array;
	int size;

public:
	Array() {
		size = 1;
		array = new int [size];
	}
	Array(int size) {
		if (size <= 0) {
			throw -1;
		}
		this->size = size;
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = 0;
		}
	}
	Array(int size, int n) {
		if (size <= 0) {
			throw -1;
		}
		this->size = size;
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = n;
		}
	}
	Array(const Array &copy) {
		this->array = new int[copy.size];
		this->size = copy.size;
		for (int i = 0; i < copy.size; i++) {
			array[i] = copy.array[i];
		}
	}
	Array(Array &&copy) {
		size = copy.size;
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = copy.array[i];
		}
		copy.size = 1;
		copy.array = new int[1];
	}
	~Array() {
		delete[] array;
		size = NULL;
	}
	int getLength();
	int& operator[](const int index);
	void resize(int newSize);
	Array& operator = (const Array &other);
	Array& operator = (Array &&other);
	bool operator ==(const Array &other);
	bool operator !=(const Array &other);
	bool operator < (const Array &other);
	bool operator > (const Array &other);
	bool operator <= (const Array &other);
	bool operator >= (const Array &other);
	Array operator +(const Array &other);
	friend ostream& operator <<(ostream &out, Array &arr);
	friend istream& operator>>(istream &in, Array &arr);
};

