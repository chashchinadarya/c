#include "pch.h"
#include "Array.h"
#include <iostream>
using namespace std;

	int Array::getLength() { return size; }
	int& Array::operator[](const int index) {
		if (index >= this->size || index < 0) {
			throw -1;
		}
		return array[index];
	}
	void Array::resize(int newSize) {
		int *newArray = new int[newSize];
		if (newSize < 0) {
			throw -1;
		}
		if (size >= newSize) {
			for (int i = 0; i < newSize; i++) {
				newArray[i] = array[i];
			}
		}
		if (size < newSize) {
			for (int i = 0; i < size; i++) {
				newArray[i] = array[i];
			}
			for (int i = size; i < newSize; i++) {
				newArray[i] = 0;
			}
		}
		delete[]array;
		size = newSize;
		this->array = newArray;
	}
	Array&Array::operator = (const Array &other) {
		delete[] this->array;
		this->array = new int[other.size];
		for (int i = 0; i < other.size; i++) {
			this->array[i] = other.array[i];
		}
		this->size = other.size;
		return *this;
	}
	Array& Array::operator = (Array &&other) {
		delete[] this->array;
		this->array = new int[other.size];
		for (int i = 0; i < other.size; i++) {
			this->array[i] = other.array[i];
		}
		this->size = other.size;
		delete[] other.array;
		other.array = new int[1];
		other.size = 1;
		return *this;
	}
	bool Array::operator == (const Array &other) {
		if (this->size != other.size) {
			throw -1;
		}
		for (int i = 0; i < other.size; i++) {
			if (other.array[i] != this->array[i]) {
				return false;
			}
		}
		return true;
	}
	bool Array::operator != (const Array &other) {
		if (this->size != other.size) {
			throw -1;
		}
		for (int i = 0; i < other.size; i++) {
			if (other.array[i] != this->array[i]) {
				return true;
			}
		}
		return false;
	}
	bool Array::operator > (const Array &other) {
		if (other.size < this->size) {
			for (int i = 0; i < other.size; i++) {
				if (other.array[i] < this->array[i]) {
					return true;
				}
			}
		}
		else {
			for (int i = 0; i < this->size; i++) {
				if (other.array[i] < this->array[i]) {
					return true;
				}
			}
		}
		return false;
	}
	bool Array::operator < (const Array &other) {
		if (other.size < this->size) {
			for (int i = 0; i < other.size; i++) {
				if (other.array[i] > this->array[i]) {
					return true;
				}
			}
		}
		else {
			for (int i = 0; i < this->size; i++) {
				if (other.array[i] > this->array[i]) {
					return true;
				}
			}
		}
		return false;
	}
	bool Array::operator >= (const Array &other) {
		if (other.size < this->size) {
			for (int i = 0; i < other.size; i++) {
				if (other.array[i] <= this->array[i]) {
					return true;
				}
			}
		}
		else {
			for (int i = 0; i < this->size; i++) {
				if (other.array[i] <= this->array[i]) {
					return true;
				}
			}
		}
		return false;
	}
	bool Array::operator <= (const Array &other) {
		if (other.size < this->size) {
			for (int i = 0; i < other.size; i++) {
				if (other.array[i] >= this->array[i]) {
					return true;
				}
			}
		}
		else {
			for (int i = 0; i < this->size; i++) {
				if (other.array[i] >= this->array[i]) {
					return true;
				}
			}
		}
		return false;
	}
	Array Array::operator +(const Array &other) {
		int newSize = this->size + other.size;
		Array *temp = new Array(newSize);
		for (int i = 0; i < this->size; i++) {
			temp->array[i] = this->array[i];
		}
		for (int j = 0; j < newSize; j++) {
			temp->array[this->size + j] = other.array[j];
		}
		temp->size = newSize;
		return *temp;
	}
	ostream& operator <<(ostream &out, Array &arr) {
		out << "����� �������: " << arr.size << endl << "������ = { ";
		for (int i = 0; i < arr.size; i++) {
			out << arr.array[i] << ' ';
		}
		out << "};" << endl;
		return out;
	}
	istream& operator>>(istream &in, Array &arr) {
		int newSize;
		cout << "������� ������" << endl;
		in >> newSize;
		if (newSize < 0) { throw - 1; }
		arr.resize(newSize);
		arr.size = newSize;
		cout << "\n������� ��������" << endl;
		for (int i = 0; i < arr.size; i++) {
			in >> arr.array[i];
		}
		return in;
	}