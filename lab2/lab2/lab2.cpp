// lab2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "Array.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	    Array *arr = new Array(5,2);
		Array *arr1 = new Array(5); // 000000
		Array *arr2 = new Array(7, 1); // 1111111
		Array *arr3 = new Array(3);// 000
		Array arr4;
		Array *arr5 = new Array(7, 1); // 1111111
		Array *arr6 = new Array(5, 1); // 11111
		Array *arr7 = new Array(5, 1); // 11111
		Array *arr8 = new Array(2, 1);
		Array *arr9 = nullptr;
		Array *arr10 = nullptr;
		arr8 = arr2;
		arr6->resize(10);
		arr7->resize(2);
		try {
			arr10 = new Array(-1,5);
		}
		catch (int a) {
			cout << "Длина не может быть отрицательной или меньше 0" << endl;
		}
		try {
			arr9 = new Array(0);
		}
		catch (int a) {
			cout << "Длина не может быть отрицательной или меньше 0" << endl;
		}

		cout << "Длина массива = " << arr->getLength() << endl;                       // Длина массива
		cout << "Получение элемента по индексу: " << arr->operator[](2) << endl;      // Оператор []
		cout << "Массив с изменённой длинной: " << *arr6 << endl;                     // Метод resize
		cout << "Массив с изменённой длинной: " << *arr7 << endl;
		cout << "Оператор =: " << *arr8 << endl;                                      // Оператор =
		try {                                                                         // Оператор ==
			if (arr->operator==(*arr3)) { cout << "Массивы равны" << endl; }
			else
			{
				cout << "Массивы не равны" << endl;
			}
		}
		catch (int a) {
			cout << "Массивы разной длины" << endl;
		}
		try {
			if (arr->operator==(*arr1)) { cout << "Массивы равны" << endl; }
			else
			{
				cout << "Массивы не равны" << endl;
			}
		}
		catch (int a) {
			cout << "Массивы разной длины" << endl;
		}
		try {
			if (arr2->operator==(*arr5)) { cout << "Массивы равны" << endl; }
			else
			{
				cout << "Массивы не равны" << endl;
			}
		}
		catch (int a) {
			cout << "Массивы разной длины" << endl;
		}
		try {                                                                                        //Оператор !=
			if (arr->operator!=(*arr3)) { cout << "Массивы не равны" << endl; }
			else
			{
				cout << "Массивы равны" << endl;
			}
		}
		catch (int a) {
			cout << "Массивы разной длины" << endl;
		}
		try {
			if (arr->operator!=(*arr1)) { cout << "Массивы не равны" << endl; }
			else
			{
				cout << "Массивы равны" << endl;
			}
		}
		catch (int a) {
			cout << "Массивы разной длины" << endl;
		}
		try {
			if (arr2->operator!=(*arr5)) { cout << "Массивы не равны" << endl; }
			else
			{
				cout << "Массивы равны" << endl;
			}
		}
		catch (int a) {
			cout << "Массивы разной длины" << endl;
		}
		cout << "Первый массив меньше второго: " << (*arr2 < *arr1) << endl;                          // Оператор <
		cout << "Первый массив меньше второго: " << (*arr1 < *arr2) << endl;
		cout << "Первый массив больше второго: " << (*arr1 > *arr2) << endl;                           // Оператор >
		cout << "Первый массив больше второго: " << (*arr2 > *arr1) << endl;
		cout << "Первый массив меньше или равен второму: " << (*arr2 <= *arr1) << endl;                          // Оператор <=
		cout << "Первый массив меньше или равен второму: " << (*arr1 <= *arr2) << endl;
		cout << "Первый массив больше или равен второму: " << (*arr1 >= *arr2) << endl;                           // Оператор >=
		cout << "Первый массив больше или равен второму: " << (*arr6 >= *arr7) << endl;
		try { cin >> arr4; }                                                                         // Оператор ввода
		catch (int a) { cout << "Размер должен быть положительным числом" << endl; }
		cout << "Вы ввели: " << arr4 << endl;                                                       // Оператор вывода
	
}

