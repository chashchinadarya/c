#include "pch.h"
#include "BufferList.h"
using namespace std;

BufferList::BufferList() {
	first = new ListD;
	first->data = 0;
	first->next = first;
	first->prev = first;
	size = 0;
}
BufferList::BufferList(const BufferList & copy) {
	first = new ListD;
	first->data = 0;
	first->next = first;
	first->prev = first;
	size = copy.size;
	ListD *buf = copy.first->next;
	for (int i = 0; i < size; i++) {
		ListD *temp = new ListD;
		temp->data = buf->data;
		temp->next = first->next;
		temp->prev = first;
		first->next = temp;
		first = temp;
		buf = buf->next;
	}
	first->next->prev = first;
	first = first->next;
}
BufferList::BufferList(BufferList&&  copy) {
	first = new ListD;
	first->data = 0;
	first->next = first;
	size = copy.size;
	copy.first = copy.first->next;
	for (int i = 0; i < this->size; i++) {
		ListD *temp = new ListD;
		temp->data = copy.first->data;
		temp->next = first->next;
		temp->prev = first;
		first->next = temp;
		first = temp;
		ListD *buf = copy.first->next;
		delete copy.first;
		copy.first = buf;
	}
	delete copy.first;
	first->next->prev = first;
	first = first->next;
}
BufferList::~BufferList(){
	clear();
	delete first;
}
BufferList& BufferList::operator= (const BufferList &copy) {
	if (this == &copy) {
		return *this;
	}
	if (size > 0) {
		clear();
	}
	size = copy.size;
	ListD *buf = copy.first->next;
	for (int i = 0; i < size; i++) {
		ListD *temp = new ListD;
		temp->data = buf->data;
		temp->next = first->next;
		temp->prev = first;
		first->next = temp;
		first = temp;
		buf = buf->next;
	}
	first->next->prev = first;
	first = first->next;
	return *this;
}
BufferList& BufferList::operator= (BufferList &&copy) {
	swap(first, copy.first);
	size = copy.size;
	clear();
	delete copy.first;
	return *this;
}

void BufferList::addElem(const TElem &elem, Iterator &iter) {
	size++;
	ListD *res = new ListD;
	res->data = elem;
	res->next = iter.getNow()->next;
	res->prev = iter.getNow();
	iter.getNow()->next->prev = res;
	iter.getNow()->next = res;
}
void BufferList::deleteElem(Iterator &iter) {
	if (size == 0) {
		throw - 1;
	}
	ListD *prev = new ListD;
	ListD *next = new ListD;
	size--;
	prev = iter.getNow()->prev;
	next = iter.getNow()->next;
	prev->next = iter.getNow()->next;
	next->prev = iter.getNow()->prev;
	free(iter.getNow());
}
void BufferList::clear() {
	first = first->next;
	while (size > 0) {
		ListD *temp = first->next;
		delete first;
		first = temp;
		size--;
	}
	first->next = first;
	first->prev = first;
}
bool BufferList::isEmpty() const {
	return (size == 0);
}

int BufferList::getSize() const {
	return size;
}
Iterator* BufferList::findElem(const TElem &elem) {
	ListIterator* list = new ListIterator(this);
	while (!list->finish()) {
		list->next();
		if (list->getElem() == elem) {
			return list;
		}
	}
	throw - 1;
}

Iterator* BufferList::getIterator() {
	return new ListIterator(this);
}
BufferList::ListIterator::ListIterator(BufferList *list) {
	this->list = list;
	this->currentNode = list->first;
}
void BufferList::ListIterator::start() {
	currentNode = list->first;
}
TElem BufferList::ListIterator::getElem() const {
	if (currentNode == list->first) {
		throw - 1;
	}
	return currentNode->data;
}
void BufferList::ListIterator::next() {
	if (finish()) {
		throw -1;
	}
	currentNode = currentNode->next;
}
void  BufferList::ListIterator::prev() {
	if (currentNode == list->first) {
		throw -1;
	}
	currentNode = currentNode->prev;
}
bool BufferList::ListIterator::finish() const {
	return (currentNode->next == list->first);
}
ListD* BufferList::ListIterator::getNow() const {
	return currentNode;
}
