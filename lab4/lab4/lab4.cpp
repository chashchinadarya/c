// lab4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "BufferList.h"
using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");
	BufferList *buf = new BufferList();
	BufferList::ListIterator iter(buf);
	buf->addElem(1, iter);
	buf->addElem(3, iter);
	buf->addElem(2, iter);
	buf->addElem(5, iter);
	buf->addElem(2, iter);
	buf->addElem(2, iter);
	buf->addElem(2, iter);
	while (!iter.finish()) {
		iter.next();
		cout << iter.getElem() << endl;
	}
	cout << "Размер = " << buf->getSize() << endl;
	cout << "Список пуст:  " << buf->isEmpty() << endl;
	try { Iterator *iter5 = buf->findElem(9); }
	catch (int a) { cout << "Такого элемента не существует" << endl; }
	Iterator *iter1 = buf->findElem(5);
	buf->deleteElem(*iter1);
	iter.start();
	while (!iter.finish()) {
		iter.next();
		cout << iter.getElem() << endl;
	}
	cout << " " << endl;
	BufferList *buf1 = new BufferList();
	BufferList::ListIterator iter2(buf1);
	buf1->addElem(2, iter2);
	buf1->addElem(2, iter2);
	buf1->addElem(2, iter2);
	buf->operator=(*buf1);
	cout << "Размер =  " << buf->getSize() << endl;
	iter.start();
	while (!iter.finish()) {
		iter.next();
		cout << iter.getElem() << endl;
	}
	buf->clear();
	cout << "Список пуст:  " << buf->isEmpty() << endl;
	Iterator *iter3 = buf1->getIterator();
	iter3->next();
	cout << iter3->getElem() << endl;
	cout << " " << endl;
	BufferList *buf2 = new BufferList(*buf1);
	BufferList::ListIterator iter4(buf2);
	iter4.start();
	while (!iter4.finish()) {
		iter4.next();
		cout << iter4.getElem() << endl;
	}
	BufferList *buf3 = new BufferList();
	BufferList::ListIterator iter5(buf3);
	try { buf->deleteElem(*iter1); }
	catch (int a) { cout << "Размер нулевой,удалить невозможно" << endl; }
}

