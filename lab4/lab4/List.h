#pragma once
#include "Iterator.h"
class List
{
public:
	virtual void addElem(const TElem &elem, Iterator &iter) = 0;
	virtual void deleteElem(Iterator &iter) = 0;
	virtual Iterator* findElem(const TElem &elem) = 0;
	virtual void makeEmpty() = 0;
	virtual bool Empty() const = 0;
	virtual int getSize() const = 0;
	virtual Iterator* getIterator() = 0;
};

