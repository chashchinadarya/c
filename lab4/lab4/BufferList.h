#pragma once
#include "List.h"
class BufferList
{private:
	ListD* first;
	int size;
public:
	friend class ListIterator;
	class ListIterator : public Iterator {
	private:
		BufferList *list;
		ListD * currentNode;
	public:
		ListIterator(BufferList *list);
		void start();
		TElem getElem() const;
		void next();
		void prev();
		bool finish() const;
		ListD* getNow() const;
	};
	BufferList();
	BufferList(const BufferList& copy);
	BufferList(BufferList&& copy);
	~BufferList();
	BufferList& operator= (const BufferList &copy);
	BufferList& operator=(BufferList&& copy);
	void addElem(const TElem &elem, Iterator &iter);
	void deleteElem(Iterator &iter);
	void clear();
	bool isEmpty() const;
	int getSize() const;
	Iterator* findElem(const TElem &elem);
	Iterator* getIterator();
};

