#pragma once
#include "iostream"
typedef int TElem;
struct ListD
{
	TElem data;
	ListD *next;
	ListD *prev;
};
class Iterator
{
public:
	virtual void start() = 0;
	virtual TElem getElem() const = 0;
	virtual void next() = 0;
	virtual void prev() = 0;
	virtual bool finish() const = 0;
	virtual ListD* getNow() const = 0;
};

