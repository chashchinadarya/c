#pragma once

template <typename Key, typename Value>
struct TElem{
private:
	Key key;
	Value value;
	TElem<Key, Value>* next;
public:
	TElem(Key key, Value value);
	Key& getKey();
	Value& getValue();
	void setValue(Value value);
	TElem<Key, Value>* getNext();
	void setNext(TElem<Key, Value>* next);
};
template <typename Key, typename Value>
class HashTable
{
	TElem<Key, Value>** array;
	int numberLayer;
public:
	HashTable(int size);
	~HashTable();
	void makeEmpty();
	bool checkEmpty();
	int hashFunc(Key& key);
	void addElem(Key key, Value value);
	void deleteElem(Key key);
	bool findElem(Key key);
	friend class Iterator;
	class Iterator {
	private:
		TElem<Key, Value>* now;
		HashTable<Key, Value>* hashTable;
		int count;
	public:
		Iterator(HashTable<Key, Value>* hashTab);
		~Iterator();
		void start();
		void hasNext();
		bool isEnd();
		Value getValue();
	};
};

