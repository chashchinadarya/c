#include "pch.h"
#include "HashTable.h"
#include <iostream>
using namespace std;
template <typename Key, typename Value>
TElem<Key,Value>::TElem(Key key, Value value) {
	this->key = key;
	this->value = value;
	this->next = nullptr;
}
template <typename Key, typename Value>
Key& TElem<Key, Value>::getKey() {
	return key;
}
template <typename Key, typename Value>
Value& TElem<Key, Value>::getValue() {
	return value;
}

template <typename Key, typename Value>
void TElem<Key, Value>::setValue(Value value){
	this->value = value;
}
template <typename Key, typename Value>
TElem<Key, Value>* TElem<Key, Value>::getNext(){
	return next;
}
template <typename Key, typename Value>
void TElem<Key, Value>::setNext(TElem<Key, Value>* next) {
	this->next = next;
}
template <typename Key, typename Value>
HashTable<Key, Value>::HashTable(int size){
	numberLayer = size;
	array = new TElem<Key, Value>*[size];
	for (int i = 0; i < size; i++) {
		array[i] = nullptr;
	}
}
template <typename Key, typename Value>
HashTable<Key, Value>::~HashTable(){
	makeEmpty();
	delete[] array;
}
template <typename Key, typename Value>
void HashTable<Key, Value>::makeEmpty() {
	TElem<Key, Value>* temp;
	TElem<Key, Value>* del;
	for (int i = 0; i < numberLayer; i++) {
		if (array[i] != nullptr) { 
			temp = array[i];
			while (temp != nullptr){
				del = temp;
				temp = temp->getNext();
				delete del;
				del = nullptr;
			}
			array[i] = nullptr;
		}
	}
}
template <typename Key, typename Value>
bool HashTable<Key, Value>::checkEmpty() {
	for (int i = 0; i < numberLayer; i++) {
		if (array[i] != nullptr) {
			return false;
		}
	}
	return true;
}
template <typename Key, typename Value>
int HashTable<Key, Value>::hashFunc(Key& key) {
	return abs((int)key) % numberLayer;
}
template <typename Key, typename Value>
void HashTable<Key, Value>::addElem(Key key, Value value) {
	int i = hashFunc(key);
	if (array[i] != nullptr) {
		TElem<Key, Value>* temp = array[i];
		while (temp->getNext() != nullptr) {
			if (temp->getKey() == key) {
				temp = nullptr;
				throw -1;
			}
			temp = temp->getNext();
		}
		TElem<Key, Value>* setEntry = new TElem<Key, Value>(key, value);
		temp->setNext(setEntry);
		temp = nullptr;
		setEntry = nullptr;
	}
	else {
		array[i] = new TElem<Key, Value>(key, value);
	}
}
template <typename Key, typename Value>
void HashTable<Key, Value>::deleteElem(Key key){
	int i = hashFunc(key);
	if (array[i] != nullptr && array[i]->getKey() != key) {
		TElem<Key, Value>* temp = array[i];
		bool flag = false;
		while (temp->getNext() != nullptr) {
			if (temp->getNext()->getKey() == key) {
				TElem<Key, Value>* del = temp->getNext();
				temp->setNext(temp->getNext()->getNext());
				del->setNext(nullptr);
				delete del;
				del = nullptr;
				flag = true;
				return;
			}
			temp = temp->getNext();
		}
		if (temp->getNext() == nullptr && !flag) {
			temp = nullptr;
			throw -1;
		}
		temp = nullptr;
	}
	else if (array[i] != nullptr && array[i]->getKey() == key) {
		TElem<Key, Value>* del = array[i];
		array[i] = array[i]->getNext();
		delete del;
		del = nullptr;
	}
	else {
		throw -1;
	}
}
template <typename Key, typename Value>
bool HashTable<Key, Value>::findElem(Key key) {
	int i = hashFunc(key);
	TElem<Key, Value>* temp = array[i];
	while (temp) {
		if (temp->getKey() == key) {
			return true;
		}
		temp = temp->getNext();
	}
	return false;
}
template <typename Key, typename Value>
HashTable<Key, Value>::Iterator::Iterator(HashTable<Key, Value>* hashTab) {
	hashTable = hashTab;
	if (hashTable->checkEmpty()) {
		throw -1;
	}
	for (count = 0; hashTable->array[count] == nullptr; count++);
	now = hashTable->array[count];
}
template <typename Key, typename Value>
HashTable<Key, Value>::Iterator::~Iterator() {
	count = 0;
	hashTable = nullptr;
	now = nullptr;
}
template <typename Key, typename Value>
void HashTable<Key, Value>::Iterator::start() {
	for (count = 0; hashTable->array[count] == nullptr; count++);
	now = hashTable->array[count];
}
template <typename Key, typename Value>
void HashTable<Key, Value>::Iterator::hasNext() {
	if (now->getNext() != nullptr) {
		now = now->getNext();
	}
	else {
		for (count++; count < hashTable->numberLayer && hashTable->array[count] == nullptr; count++);
		if (count == hashTable->numberLayer) {
			now = nullptr;
		}
		else {
			now = hashTable->array[count];
		}
	}
}
template <typename Key, typename Value>
bool HashTable<Key, Value>::Iterator::isEnd() {
	return now == nullptr;
}
template <typename Key, typename Value>
Value HashTable<Key, Value>::Iterator::getValue() {
	return now->getValue();
}