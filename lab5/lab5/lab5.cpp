#include "pch.h"
#include <iostream>
#include "HashTable.h"
#include "HashTable.cpp"
using namespace std;

int main(){
	setlocale(LC_ALL, "Russian");
	HashTable<int, int>* hashTab = new HashTable<int, int>(6);
	hashTab->addElem(10, 12); // hach = 4
	hashTab->addElem(8, 5);   // hash = 2
	hashTab->addElem(-4, 62); // hash = 5
	hashTab->addElem(1, 100); // hash = 1
	hashTab->addElem(14, 87); // hash = 2
	hashTab->addElem(6, 3);   // hash = 0
	hashTab->addElem(0, 9);   // hash = 0
	try {
		hashTab->addElem(10, 12); // hash = 2
	}
	catch (int a) { cout << "Элемент с таким ключом уже существует" << endl; }

	HashTable<int, int>::Iterator itr(hashTab);
	itr.start();
	while (!itr.isEnd()) {
		cout << itr.getValue() << " ";
		itr.hasNext();
	}
	cout << endl;
	cout << "Элемент найден: " << hashTab->findElem(10) << endl;
	hashTab->deleteElem(10);
	hashTab->deleteElem(-4);
	itr.start();
	while (!itr.isEnd()) {
		cout << itr.getValue() << " ";
		itr.hasNext();
	}
	cout << " " << endl;
	cout << "Элемент найден: " << hashTab->findElem(10) << endl;
	cout << "Таблица пуста: " << hashTab->checkEmpty() << endl;
	hashTab->makeEmpty();
	cout << "Таблица пуста: " << hashTab->checkEmpty() << endl;
}

