#pragma once
#include "iostream"
#include "string"
using namespace std;


struct TElem{//���� ������
	string word;
	int frequency;
	TElem *right, *left;
	TElem(string word, int frequency = 1, TElem *right = nullptr, TElem *left = nullptr);
};
class DictionaryTree
{
private:
	TElem* root;
    int size;
public:
	DictionaryTree();
	DictionaryTree(const DictionaryTree &copy);
	DictionaryTree(DictionaryTree &&move);
	DictionaryTree& operator=(const DictionaryTree &copy);
	DictionaryTree& operator=(DictionaryTree &&move);
	~DictionaryTree();
	void copyTree(TElem *root, TElem *copy);
	void deleteTree(TElem *root);
	int findWord(TElem *root, string word);
	void addWord(TElem *&root, string word);
	bool deleteWord(TElem *&root, string word);
	void delTwo(TElem *& leftroot, TElem *& del);
	int totalNumber();
	friend ostream& operator<< (ostream& os, DictionaryTree &obj);
	void print(ostream &out, TElem *root);
	int findWord(string word);
	void addWord(string word);
	void deleteWord(string word);
};

