#include "pch.h"
#include "DictionaryTree.h"

TElem::TElem(string word, int frequency, TElem *right, TElem *left) {
	this->word = word;
	this->frequency = frequency;
	this->left = left;
	this->right = right;
}
DictionaryTree::DictionaryTree(){
	root = nullptr;
	size = 0;
}
DictionaryTree::DictionaryTree(const DictionaryTree &copy) {
	copyTree(root, copy.root);
	size = copy.size;
}
DictionaryTree::DictionaryTree(DictionaryTree &&copy) {
	swap(root, copy.root);
	size = copy.size;
}
DictionaryTree::~DictionaryTree(){
	deleteTree(root);
}
DictionaryTree& DictionaryTree::operator=(const DictionaryTree &copy) {
	if (this == &copy) {
		return *this;
	}
	deleteTree(root);
	root = nullptr;
	size = copy.size;
	copyTree(root, copy.root);
}
DictionaryTree& DictionaryTree::operator=(DictionaryTree &&copy) {
	swap(root, copy.root);
	size = copy.size;
	copy.deleteTree(copy.root);
	return *this;
}
void DictionaryTree::copyTree(TElem *root, TElem *copy) {
	if (!copy) {
		return;
	}
	root = new TElem(copy->word, copy->frequency);
	copyTree(root->left, copy->left);
	copyTree(root->right, copy->right);
}
void DictionaryTree::deleteTree(TElem *root) {
	if (!root) return; // ������� ������
	deleteTree(root->left);
	deleteTree(root->right);
	delete root;
}
void DictionaryTree::delTwo(TElem *& leftroot, TElem *& del) {
	if (leftroot->right) {
		delTwo(leftroot->right, del);
	}
	del->word = leftroot->word;
	del->frequency = root->frequency;
	del = leftroot;
	leftroot = leftroot->left;
}
void DictionaryTree::print(ostream &out, TElem *root) {
	if (!root) { // ���� ����� �� �����
		return; // ����� ������ �� ������
	}
	print(out, root->left); // ����������� ����� ���������
	out << root->word << "---" << root->frequency << endl; // ����������� �������
	print(out, root->right); // ����������� ������ ���������
}
int DictionaryTree::findWord(TElem *root, string word) {
	if (!root) { return 0; }
	if (root->word.compare(word) == 0) {
		return root->frequency;
	}
	if (root->word.compare(word) > 0) {
		return findWord(root->left, word);
	}
	if (root->word.compare(word) < 0) {
		return findWord(root->right, word);
	}
}
void DictionaryTree::addWord(TElem *&root, string word) {
	if (!root) {
		root = new TElem(word);
		root->left = nullptr;
		root->right = nullptr;
		return;
	}
	if (root->word.compare(word) == 0) {
		root->frequency++;
		return;
	}
	if (root->word.compare(word) > 0) {
		addWord(root->left, word);
	}
	if (root->word.compare(word) < 0) {
		addWord(root->right, word);
	}
}
bool DictionaryTree::deleteWord(TElem *&root, string word) {
	if (!root) {
		return false;
	}
	if (root->word.compare(word) > 0) {
		if (deleteWord(root->left, word)) { return true; };
		return false;
	}
	if (root->word.compare(word) < 0) {
		if (deleteWord(root->right, word)) { return true; }
		return false;
	}
	TElem *wordDel = root;//����� �����, ������� ���� �������
	if (root->frequency > 1) {
		root->frequency--;
		return true;
	}
	else {
		if (!root->right) {
			root = root->left;
			delete wordDel;
			return true;
		}
		if (!root->left) {
			root = root->right;
			delete wordDel;
			return true;
		}
		delTwo(root->left, wordDel);
		delete wordDel;
		return true;
	}
	return false;
}
int DictionaryTree::totalNumber() {
	return size;
}
ostream& operator<< (ostream& out, DictionaryTree &obj) {
	obj.print(out, obj.root);
	return out;
}
int DictionaryTree::findWord(string word) {
	return findWord(root, word);
}
void DictionaryTree::addWord(string word) {
	size++;
	addWord(root, word);
}
void  DictionaryTree::deleteWord(string word) {
	if (deleteWord(root, word)) {
		size--;
	}
	else { throw - 1; }
}
