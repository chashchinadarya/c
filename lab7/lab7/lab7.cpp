#include "pch.h"
#include <iostream>
#include "DictionaryTree.h"
using namespace std;
int main(){
	setlocale(LC_ALL, "rus");
	DictionaryTree *dictionaryTree = new DictionaryTree();
/*1*/	dictionaryTree->addWord("aaa");
/*2*/	dictionaryTree->addWord("bbbbbbbb");
/*3*/	dictionaryTree->addWord("cbcbcbc");
/*4*/	dictionaryTree->addWord("aaaf");
/*5*/	dictionaryTree->addWord("aaba");
/*6*/	dictionaryTree->addWord("aaa");
/*7*/	dictionaryTree->addWord("aaa");
/*8*/	dictionaryTree->addWord("ccc");
/*9*/	dictionaryTree->addWord("aaa");
/*10*/	dictionaryTree->addWord("bbcbbbb");
/*11*/	dictionaryTree->addWord("bbbbbbbb");
/*12*/	dictionaryTree->addWord("fff");
/*13*/	dictionaryTree->addWord("fff");
/*14*/	dictionaryTree->addWord("ccc");
/*15*/	dictionaryTree->addWord("aaa");
	cout << *dictionaryTree << endl;
	cout << "Всего слов в словаре: " << dictionaryTree->totalNumber() << endl;
	cout << "Количество вхождений слова =  " << dictionaryTree->findWord("aaa") << endl;
	dictionaryTree->deleteWord("aaaf");
	dictionaryTree->deleteWord("aaa");
	try { dictionaryTree->deleteWord("aaaf"); }
	catch (int a) { cout << "Такого слова нет в словаре" << endl; }
	cout << *dictionaryTree << endl;
	cout << "Всего слов в словаре: " << dictionaryTree->totalNumber() << endl;
	cout << "Количество вхождений слова =  " << dictionaryTree->findWord("aaa") << endl;
	DictionaryTree *dictionaryTree1 = dictionaryTree;
	cout << *dictionaryTree1 << endl;
}
