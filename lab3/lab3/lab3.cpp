#include "pch.h"
#include <iostream>
#include "Iterator.h"
#include "RingBuffer.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	try { RingBuffer *ringD = new RingBuffer(-5); }
	catch (int a) {
		cout << "Неккоректный размер" << endl;
	}
	RingBuffer *ringB = new RingBuffer(5);
	ringB->addElem(1);
	ringB->addElem(2);
	ringB->addElem(3);
    RingBuffer *ringA = new RingBuffer(*ringB);
	RingBuffer *ringC = new RingBuffer(2);
	ringA->addElem(0);
	cout << "Изъяли элемент из очереди: " << ringA->getElem() << endl;
	cout << "Прочитали элемент из очереди: " << ringA->readElem() << endl;
	cout << "Размер очереди: " << ringA->getSize() << endl;
	ringA->makeEmpty();
	cout << "Очередь пуста: " << ringA->checkEmpty() << endl;
	cout << "Очередь пуста: " << ringB->checkEmpty() << endl;
	try { ringC->getElem(); }
	catch (int a) { cout << "Очередь пуста" << endl; }
	try { ringC->readElem(); }
	catch (int a) { cout << "Очередь пуста" << endl; }

	ringC->addElem(3);
	ringC->addElem(3);
	try {
		ringC->addElem(3);
	}
	catch (int a) {
		cout << "Очередь переполнена" << endl;
	}
	Iterator iteratorA = Iterator(*ringA); 
	
	Iterator iteratorB = Iterator(*ringB);
	try { iteratorA.getValue(); }
	catch (int a) { cout << " ----------" << endl; }
	while (!iteratorB.finish()) {
		iteratorB.next();
		cout << iteratorB.getValue() << endl;
	}
}

