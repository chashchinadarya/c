#include "pch.h"
#include "RingBuffer.h"


RingBuffer::RingBuffer(){
	this->buffer = new int[10];
	this->size = 10;
	this->head = 0;
	this->tail = 0;
	this->empty = true;
}
RingBuffer::RingBuffer(int size) {
	if (size <= 0) { throw - 1; }
	this->buffer = new int[size];
	this->size = size;
	this->head = 0;
	this->tail = 0;
	this->empty = true;
}
RingBuffer::RingBuffer(const RingBuffer& copy) {
	this->size = copy.size;
	this->head = copy.head;
	this->tail = copy.tail;
	this->empty = copy.empty;
	this->buffer = new int[copy.size];
	for (int i = 0; i < copy.size; i++) {
		this->buffer[i] = copy.buffer[i];
	}
}
RingBuffer::~RingBuffer(){
	delete[]buffer;
}
void RingBuffer::addElem(int elem) {
	if (((tail + 1) % size) == head) {
		throw - 1;
	}
	if (empty) {
		this->buffer[tail] = elem;
		this->empty = false;
	}
	else {
		this->tail = (tail + 1) % size;
		this->buffer[tail] = elem;
	}
}
int RingBuffer::getElem() {
	if (empty) {
		throw - 1;
	}
	if (this->head == this->tail) {
		this->empty = true;
		return this->buffer[head];
	}
	else {
		this->head++;
		return this->buffer[head - 1];
	}
}
int RingBuffer::readElem() {
	if (this->empty) {
		throw - 1;
	}
	return this->buffer[head];
}
int RingBuffer::getSize() {
	return size;
}
void RingBuffer::makeEmpty() {
	this->empty = true;
	this->head = this->tail;
}
bool RingBuffer::checkEmpty() { return this->empty; }
