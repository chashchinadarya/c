#pragma once
#include "RingBuffer.h"

class Iterator
{
private:
	RingBuffer *buffer;
	int index;
public:
	Iterator(RingBuffer buffer);
	~Iterator();
	void start();
	void next();
	bool finish();
	int getValue();
};

