#include "pch.h"
#include "Iterator.h"


Iterator::Iterator(RingBuffer buffer){
	this->buffer = new RingBuffer(buffer);
	this->index = buffer.head;
}

Iterator::~Iterator()
{
}

void Iterator::start() {
	index = buffer->head;
}
void Iterator::next() {
	if (this->finish()) {
		throw -1;
	}
	index = (index + 1) % buffer->size;
}
bool Iterator::finish() {
	return (index == (buffer->tail % buffer->size));
}
int Iterator::getValue() {
	if (this->finish()) {
		throw - 1;
	}
	return buffer->buffer[index];
}