#pragma once
class RingBuffer
{
private:
	int *buffer;
	int size;
	int head;
	int tail;
	bool empty;
public:
	RingBuffer();
	RingBuffer(int size);
	RingBuffer(const RingBuffer& copy);
	~RingBuffer();
	void addElem(int elem);
	int getElem();
	int readElem();
	int getSize();
	void makeEmpty();
	bool checkEmpty();
	friend class Iterator;
};

