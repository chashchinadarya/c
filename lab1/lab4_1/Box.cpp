#include "pch.h"
#include <iostream>
#include "Box.h"

using namespace std;
namespace BoxContainer {
	int Box::getLength() { return length; }
	void Box::setLength(int len) { length = len; }
	double Box::getWeight() { return weight; }
	void Box::setWeight(int wei) { weight = wei; }
	int Box::getHeight() { return height; }
	void Box::setHeight(int hei) { height = hei; }
	int Box::getWidth() { return width; }
	void Box::setWidth(int wid) { width = wid; }
	int Box::getValue() { return value; }
	void Box::setValue(int val) { value = val; }

	double sumBox(Box box[], int size) {
		int summa = 0;
		for (int i = 0; i < size; i++) {
			summa = summa + box[i].getValue();
		}
		return summa;
	}
	bool checkBox(Box box[], int size, int value) {
		int summa = 0;
		for (int i = 0; i < size; i++) {
			summa = summa + box[i].getLength() + box[i].getWidth() + box[i].getHeight();
		}
		if (summa <= value) {
			return true;
		}
		else { return false; }
	}
	double maxWeight(Box box[], int size, int maxV) {
		double maxWeight = 0;
		for (int i = 0; i < size; i++) {
			if ((box[i].getLength() * box[i].getWidth() * box[i].getHeight()) <= maxV) {
				if (box[i].getWeight() > maxWeight) {
					maxWeight = box[i].getWeight();
				}
			}
		}
		return maxWeight;
	}
	bool insertionBox(Box box[], int size) {
		Box temp;
		for (int i = 0; i < size - 1; i++) {
			for (int j = 0; j < size - i - 1; j++) {
				if (box[j].getLength() > box[j + 1].getLength()) {
					temp = box[j];
					box[j] = box[j + 1];
					box[j + 1] = temp;
				}
			}
		}
		for (int j = 0; j < size - 1; j++) {
			if (box[j].getLength() == box[j + 1].getLength()) { return false; }
		}
		for (int k = 0; k < size - 1; k++) {
			if (box[k].getWidth() == box[k + 1].getWidth() || box[k].getWidth() > box[k + 1].getWidth()) {
				return false;
			}
		}
		for (int n = 0; n < size - 1; n++) {
			if (box[n].getHeight() == box[n + 1].getHeight() || box[n].getHeight() > box[n + 1].getHeight()) {
				return false;
			}
		}
		return true;
	}
	bool operator == (Box b1, Box b2) {
		return (b1.getLength() == b2.getLength() &&
			b1.getWidth() == b2.getWidth() &&
			b1.getHeight() == b2.getHeight() &&
			b1.getWeight() == b2.getWeight() &&
			b1.getValue() == b2.getValue());
	}
	ostream& operator <<(ostream &out, Box &b1)
	{
		out << "����� �������:" << b1.getLength() << endl;
		out << "������ �������:" << b1.getWidth() << endl;
		out << "������ �������:" << b1.getHeight() << endl;
		out << "��� �������:" << b1.getWeight() << endl;
		out << "��������� �������:" << b1.getValue() << endl;
		return out;
	}
	istream& operator>>(istream &in, Box &b1)
	{
		in >> b1.length;
		in >> b1.width;
		in >> b1.height;
		in >> b1.weight;
		in >> b1.value;
		return in;
	}
}