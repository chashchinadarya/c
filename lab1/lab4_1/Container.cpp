#include "pch.h"
#include <iostream>
#include "Container.h"
#include <vector>
#include "Box.h"

namespace BoxContainer {
	int Container::getLength() { return length; }
	void Container::setLength(int len) { length = len; }
	double Container::getMaxWeight() { return maxWeight; }
	void Container::setMaxWeight(int wei) { maxWeight = wei; }
	int Container::getHeight() { return height; }
	void Container::setHeight(int hei) { height = hei; }
	int Container::getWidth() { return width; }
	void Container::setWidth(int wid) { width = wid; }

	int Container::quantityBox() {
		return container.size();
	}
	double Container::sumWeight() {
		double sum = 0;
		for (int i = 0; i < container.size(); i++) {
			sum += container[i].getWeight();
		}
		return sum;
	}
	int Container::sumValue() {
		int sum = 0;
		for (int i = 0; i < container.size(); i++) {
			sum += container[i].getValue();
		}
		return sum;
	}
	Box Container::indexBox(int index) {
		return container[index];
	}
	int Container::addBox(Box box) {
		if (Container::sumWeight() + box.getWeight() <= Container::getMaxWeight()) {
			container.push_back(box);
			return container.size();
		}
		else
		{
			throw -1;
		}
	}
	void Container::deleteBox(int index) {
		container.erase(container.begin() + index);
	}
	Box& Container::operator[](const int index) {
		return container[index];
	}

	ostream& operator <<(ostream &out, Container &cv)
	{
		out << "����� ����������:" << cv.getLength() << endl;
		out << "������ ����������:" << cv.getWidth() << endl;
		out << "������ ����������:" << cv.getHeight() << endl;
		out << "��� ����������:" << cv.getMaxWeight() << endl;
		return out;
	}
	istream& operator>>(istream &in, Container &cv)
	{
		in >> cv.length;
		in >> cv.width;
		in >> cv.height;
		in >> cv.maxWeight;
		return in;
	}
}




