#pragma once
#include "pch.h"
#include <iostream>
using namespace std;
namespace BoxContainer {
	class Box {
	private:
		int length;
		int width;
		int height;
		double weight;
		int value;
	public:
		Box(int length, int width, int height, double weight, int value) {
			this->length = length;
			this->weight = weight;
			this->height = height;
			this->width = width;
			this->value = value;
		}
		Box() {
			this->length = 1;
			this->weight = 1;
			this->height = 1;
			this->width = 1;
			this->value = 0;
		}
		int getLength();
		void setLength(int len);
		double getWeight();
		void setWeight(int wei);
		int getHeight();
		void setHeight(int hei);
		int getWidth();
		void setWidth(int wid);
		int getValue();
		void setValue(int val);
		friend ostream& operator <<(ostream &out, Box &b1);
		friend istream& operator>>(istream &in, Box &b1);
	};

	double sumBox(Box box[], int size);
	bool checkBox(Box box[], int size, int value);
	double maxWeight(Box box[], int size, int maxV);
	bool insertionBox(Box box[], int size);
	bool operator == (Box b1, Box b2);

}
