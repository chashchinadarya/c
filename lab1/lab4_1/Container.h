#pragma once
#include <vector>
#include "Box.h"
using namespace std;
namespace BoxContainer {
	class Container {
	private:
		int length;
		int width;
		int height;
		double maxWeight;
	public:
		vector <Box> container;
		Container(int length, int width, int height, double maxWeight) {
			this->length = length;
			this->width = width;
			this->height = height;
			this->maxWeight = maxWeight;
		}
		Container() {
			this->length = 1;
			this->width = 1;
			this->height = 1;
			this->maxWeight = 1;

		}
		int getLength();
		void setLength(int len);
		double getMaxWeight();
		void setMaxWeight(int wei);
		int getHeight();
		void setHeight(int hei);
		int getWidth();
		void setWidth(int wid);

		int quantityBox();
		double sumWeight();
		int sumValue();
		Box indexBox(int index);
		int addBox(Box box);
		void deleteBox(int index);
		Box& operator[](const int index);
		friend ostream& operator <<(ostream &out, Container &cv);
		friend istream& operator>>(istream &in, Container &cv);
	};
}




