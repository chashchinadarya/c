#include "pch.h"
#include <iostream>
#include "module.h"

using namespace std;

double sumBox(Box box[], int size) {
	int summa = 0;
	for (int i = 0; i < size; i++) {
		summa = summa + box[i].value;
	}
	return summa;
}
bool checkBox(Box box[], int size, int value) {
	int summa = 0;
	for (int i = 0; i < size; i++) {
		summa = summa + box[i].length + box[i].width + box[i].height;
	}
	if (summa <= value) {
		return true;
	}
	else { return false; }
}
double maxWeight(Box box[], int size, int maxV) {
	double maxWeight = 0;
	for (int i = 0; i < size; i++) {
		if ((box[i].length * box[i].width * box[i].height) <= maxV) {
			if (box[i].weight > maxWeight) {
				maxWeight = box[i].weight;
			}
		}
	}
	return maxWeight;
}
bool insertionBox(Box box[], int size) {
	Box temp;
	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			if (box[j].length == box[j + 1].length) { return false; }
			else if (box[j].length > box[j + 1].length) {
				temp = box[j];
				box[j] = box[j + 1];
				box[j + 1] = temp;
			}
		}
		for (int k = 0; k < size - 1; k++) {
			if (box[k].width == box[k + 1].width || box[k].width > box[k + 1].width) {
				return false;
			}
			else {
				for (int n = 0; n < size - 1; n++) {
					if (box[n].height == box[n + 1].height || box[n].height > box[n + 1].height) {
						return false;
					}
					else {
						return true;
					}
				}
			}
		}
	}
}
bool operator == (Box b1, Box b2) {
	return (b1.length == b2.length &&
		b1.width == b2.width &&
		b1.height == b2.height &&
		b1.weight == b2.weight &&
		b1.value == b2.value);
}
ostream& operator <<(ostream &out, Box &b1)
{
	out << "����� �������:" << b1.length << endl;
	out << "������ �������:" << b1.width << endl;
	out << "������ �������:" << b1.height << endl;
	out << "��� �������:" << b1.weight << endl;
	out << "��������� �������:" << b1.value << endl;
	return out;
}
istream& operator>>(istream &in, Box &b1)
{
	in >> b1.length;
	in >> b1.width;
	in >> b1.height;
	in >> b1.weight;
	in >> b1.value;
	return in;
}