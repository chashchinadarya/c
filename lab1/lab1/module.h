#pragma once
#include "pch.h"
#include <iostream>

using namespace std;

struct Box {
	int length;
	int width;
	int height;
	double weight;
	int value;
	Box(int length, int width, int height, double weight, int value) {
		this->length = length;
		this->weight = weight;
		this->height = height;
		this->width = width;
		this->value = value;
	}
	Box() {
		this->length = 1;
		this->weight = 1;
		this->height = 1;
		this->width = 1;
		this->value = 0; 
	}
};
double sumBox(Box box[], int size);
bool checkBox(Box box[], int size, int value);
double maxWeight(Box box[], int size, int maxV);
bool insertionBox(Box box[], int size);
bool operator == (Box b1, Box b2);
ostream& operator <<(ostream &out, Box &b1);
istream& operator>>(istream &in, Box &b1);
